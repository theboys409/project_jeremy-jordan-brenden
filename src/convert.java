
public class convert 
{   
    /*
     @Author: Jeremy Philips
    Jordan Nordh
     Brenden Bickner
     */
    
    
	private double FahrenheitToCelcius(double F)
	{
		return (F-32)*5/9;
	}
	
	private double CelciusToFahrenheit(double C)
	{
		return (C*1.8)+32;
	}
	
	public static void main(String[] args)
	{
		convert c=new convert();
		System.out.println("180 Celcius in Fahrenheit is " + c.CelciusToFahrenheit(180));
		System.out.println("250 Fahrenheit in Celcius is " + c.FahrenheitToCelcius(250));	
	}
}
